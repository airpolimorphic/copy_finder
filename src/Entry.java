import controller.Controller;
import model.exceptions.UnexpectedException;
import model.unit.AudioCore;
import model.unit.Settings;
import model.utils.FileUtil;
import view.Progress;

import java.io.File;
import java.util.Scanner;

public class Entry {

    private static final String SETTINGS_PATH = "./settings";

    public static void main(String... args){

        Settings settings = new Settings();
        FileUtil utils = new FileUtil();

         if(new File(SETTINGS_PATH).exists()){
             try {
                 settings = utils.loadSetting(SETTINGS_PATH);
             }catch (UnexpectedException e){
                 e.printStackTrace();
                 System.exit(1);
             }
            Controller.start(settings);
        }else {


            String source = "";
            String dest = "";
            int numCores = 2;
            int numPhotos = 1000;
            boolean deleteCopies = false;

            Scanner scanner = new Scanner(System.in);
            System.out.println("Type path of folder, where do yo want FIND copies:");
            source = scanner.nextLine();
            try {
                source = align(source);
            } catch (Exception e) {
                e.printStackTrace();
                //ignoring
            }
            System.out.println();
            System.out.println();
            System.out.println("Type path of folder, where do you want STORE copies:");
            dest = scanner.nextLine();
            try {
                dest = align(dest);
            } catch (Exception e) {
                e.printStackTrace();
            }

            System.out.println();
            System.out.println();
            System.out.println("Type number of core, which you want to use:");
            numCores = scanner.nextInt();
            System.out.println();
            System.out.println();
            System.out.println("Type number of images-buffer, which be loaded in RAM, number in images:");
            numPhotos = scanner.nextInt();
            System.out.println("\r\n\r\n");
            System.out.println("Delete copyes?");
            deleteCopies = scanner.nextBoolean();

            settings.setDeleteCopies(deleteCopies);
            settings.setNumBlocks(numPhotos);
            settings.setNumCores(numCores);
            settings.setPathFrom(source);
            settings.setPathTo(dest);

            utils.save(settings, SETTINGS_PATH);
            Controller.start(settings);

        }

        System.gc();
        AudioCore.instance().close();

        Progress progress = new Progress();
        progress.setSize(utils.getDeleteList().size());

        System.out.println("Deleting copies");
        for(String path:utils.getDeleteList()) {
            progress.step();
            if (path == null || path.isEmpty()) continue;
            File file = new File(path);
            if (file.exists()) {
                file.delete();
            }

        }

        utils.clear();
        new File(SETTINGS_PATH).delete();
    }



    private static String align(String path){
        if(path.startsWith("file:///")){
            path = path.replace("file:///", "/");
        }
        if(path.endsWith(" ")){
            path = path.substring(0, path.length()-1);
        }
        return path;
    }





}
