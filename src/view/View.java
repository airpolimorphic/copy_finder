package view;

import view.data.provider.Data;

public class View {
    //region SINGLETON

    private static View view = new View();
    private View(){

    }
    public static View instance(){
        return view;
    }

    //endregion

    public void print(Data data){
        data.print();
    }

}
