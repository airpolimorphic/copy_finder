package view;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

public class Progress implements Serializable {

    AtomicLong counter = new AtomicLong(0);
    AtomicLong percent = new AtomicLong(0);
    Set<Long> history = Collections.synchronizedSet(new HashSet<Long>());
    long size = 0;

    public void setSize(long size){
        this.size = size;
        counter.set(0);
        percent.set(0);
    }

    public void step(){
        long counterS = counter.incrementAndGet();
       show(counterS);
    }

    public void step(String info){
        long counterS = counter.incrementAndGet();
        show(counterS, info);
    }

    public void step(long pos){
        long counterS =  counter.addAndGet(pos);
       show(counterS);
    }

    public void setStep(long pos){
        counter.set(pos);
        show(pos);
    }

    private void show(long value){
        long newPercent = (value*100)/size;
        if (newPercent!=percent.get()){
            if(history.add(newPercent)) {
                System.out.print(newPercent + "% ");
                if(newPercent<10)System.out.print(" ");
                if (newPercent % 25 == 0) {
                    System.out.println();
                }
                percent.set(newPercent);
            }
        }
    }

    private void show(long value, String info){
        long newPercent = (value*100)/size;
        if (newPercent!=percent.get()){
            if(history.add(newPercent)) {
                System.out.print(newPercent + "% "+info+" ");
                if(newPercent<10)System.out.print(" ");
                if (newPercent % 25 == 0) {
                    System.out.println();
                }
                percent.set(newPercent);
            }
        }
    }


}
