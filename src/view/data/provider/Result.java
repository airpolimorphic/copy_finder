package view.data.provider;

public class Result implements Data{
    private int totalMaches;
    private int filesDeleted;
    private long deletedFileSize;
    private long getTotalMachesSize;
    private int errorsCount;
    private static final double KBYTE = 1024;
    private static final double MBYTE = KBYTE * KBYTE;
    private static final double GBYTE = MBYTE * KBYTE;


    public void setTotalMaches(int totalMaches) {
        this.totalMaches = totalMaches;
    }

    public void setFilesDeleted(int filesDeleted) {
        this.filesDeleted = filesDeleted;
    }

    public void setDeletedFileSize(long deletedFileSize) {
        this.deletedFileSize = deletedFileSize;
    }

    public void setTotalMachesSize(long getTotalMachesSize) {
        this.getTotalMachesSize = getTotalMachesSize;
    }

    public void setErrorsCount(int errorsCount) {
        this.errorsCount = errorsCount;
    }

    @Override
    public void print() {

        System.out.println(this);
    }

    public String toString(){
        StringBuilder out = new StringBuilder();
        out
                .append("\r\nTotal matches: ").append(totalMaches)
                .append(" Maches size: ").append(humanize(getTotalMachesSize, GBYTE))
                .append("\r\nDeleted files: ").append(filesDeleted)
                .append(" Deleted size: ").append(humanize(deletedFileSize, GBYTE))
                .append("\r\nErrors: ").append(errorsCount);
        return out.toString();
    }

    private String humanize(long size, double delimeter){
        boolean isGB = delimeter==GBYTE;
        boolean isMB = delimeter==MBYTE;
        boolean isKB = delimeter==KBYTE;
        String out;
        double result = size / delimeter;
        if(result<1){
            if(isGB) return humanize(size, MBYTE);
            if(isMB) return humanize(size, KBYTE);
        }
        out = result+"";
        if(isGB) out+=" GB";
        if(isMB) out+=" MB";
        if(isKB) out+=" KB";
        return out;
    }
}
