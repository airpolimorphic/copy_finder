package view.data.provider;

public interface Data {
    void print();

    default void show(String info){
        System.out.println(info);
    }
}
