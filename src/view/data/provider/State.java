package view.data.provider;

import model.unit.PlayTime;

public class State implements Data {

    private long totalCombinations;
    private long currentCombinations;

    private long spentTime;
    private double timePerCombination;

    private long elapsedCombinations;
    private long remainCombinations;
    private String timeRemain = "";

    public void setTotalCombinations(long totalCombinations) {
        this.totalCombinations = totalCombinations;
    }

    public void setCurrentCombinations(long currentCombinations) {
        this.currentCombinations = currentCombinations;

    }

    public void setSpentTime(long spentTime) {
        this.spentTime = spentTime;
        if(currentCombinations!=0)
            timePerCombination = (double)spentTime / currentCombinations;
    }

    public void setElapsedCombinations(long elapsedCombinations){
        this.elapsedCombinations = elapsedCombinations;
    }

    @Override
    public void print() {
        elapsedCombinations+=currentCombinations;
        remainCombinations = totalCombinations - elapsedCombinations;
        if(timePerCombination!=0){
            long timeRemain = (long)(remainCombinations*timePerCombination);
            PlayTime playTime = new PlayTime(timeRemain);
            this.timeRemain = playTime.getDisplayedTimeAtLine();
        }
        System.out.println(this);
    }



    public String toString(){
        StringBuilder out = new StringBuilder();
        out
                .append("\r\n\r\nCombinations { ")
                .append("Total: ").append(totalCombinations)
                .append(" Current: ").append(currentCombinations)
                .append(" Elapsed: ").append(elapsedCombinations)
                .append(" Remain: ").append(remainCombinations)
                .append("}")
                .append(" Time remain: ").append(timeRemain);
        return out.toString();
    }
}
