package view.data.provider;

public class TechnicInfo implements Data{

    private int totalFiles;
    private int toLoadSize;
    private int toDeleteSize;
    private int historySize;

    public void setTotalFiles(int totalFiles) {
        this.totalFiles = totalFiles;
    }

    public void setToLoadSize(int toLoadSize) {
        this.toLoadSize = toLoadSize;
    }

    public void setToDeleteSize(int toDeleteSize) {
        this.toDeleteSize = toDeleteSize;
    }

    public void setHistorySize(int historySize) {
        this.historySize = historySize;
    }

    @Override
    public void print() {



    }
}
