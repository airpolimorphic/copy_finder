package controller;

import model.CopyDetector;
import model.unit.Settings;
import model.utils.FileUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.UUID;


public class Controller {

	public static void start(Settings settings) {
		FileUtil util = new FileUtil();
		String copyPath = settings.getPathTo();
		CopyDetector detector = new CopyDetector().setNumThreads(settings.getNumCores()).setNumUnitsInBlock(settings.getNumBlocks());
		detector.setDeleteCopies(settings.isDeleteCopies());
		ArrayList<File> sourceFiles = util.getImages(new File(settings.getPathFrom()).getAbsolutePath());
		copyPath = copyPath+"/"+ UUID.randomUUID().toString().replaceAll("-", "");
		detector.start(sourceFiles,  copyPath, util);
	}

}
