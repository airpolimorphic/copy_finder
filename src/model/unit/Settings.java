package model.unit;

import java.io.Serializable;

public class Settings implements Serializable{
    private String pathFrom;
    private String pathTo;
    private int numCores = 4;
    private int numBlocks = 1000;
    private boolean deleteCopies = false;

    public String getPathFrom() {
        return pathFrom;
    }

    public void setPathFrom(String pathFrom) {
        this.pathFrom = pathFrom;
    }

    public String getPathTo() {
        return pathTo;
    }

    public void setPathTo(String pathTo) {
        this.pathTo = pathTo;
    }

    public int getNumCores() {
        return numCores;
    }

    public void setNumCores(int numCores) {
        this.numCores = numCores;
    }

    public int getNumBlocks() {
        return numBlocks;
    }

    public void setNumBlocks(int numBlocks) {
        this.numBlocks = numBlocks;
    }

    public boolean isDeleteCopies() {
        return deleteCopies;
    }

    public void setDeleteCopies(boolean deleteCopies) {
        this.deleteCopies = deleteCopies;
    }
}
