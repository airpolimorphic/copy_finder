package model.unit;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.*;

public class Generator implements Iterable<int[][]>, Serializable {


    private int totalSize;
    private int blockSize;
    int step;

    private int initX;
    private int initY = -1;

    private Deque<Integer> cash = new ArrayDeque<>();
    private Set<Integer> history = new HashSet<>();

    private transient Iterator<Integer> subIterator;
    private int iteratorPosition;

    private long combintaionsElapsed;

    public void setTotalSize(int totalSize) {
        this.totalSize = totalSize;
    }

    public void setBlockSize(int blockSize) {
        this.blockSize = blockSize;
        step = blockSize -1;
    }

    //region extra block
    public boolean hasExtraNext(){
        boolean xResult = initX<totalSize&&!history.contains(initX);
        boolean yResult = initY<totalSize-1&&initY!=-1;
        return xResult||yResult;
    }

    public Deque<Integer> generateExtraNext(){
            if(initY<totalSize-1&&initY!=-1){
                cash.add(initY);
                history.add(initY);
                initY+=step;
                //change y
            }else{
                cash = new ArrayDeque<>();
                cash.add(initX);
                initY=initX+step;
                initX++;
                //change x
            }
            return cash; //last index is last value;
    }
    //endregion

    public void load() {
        while (iteratorPosition>0) {
            generateNext();
            iteratorPosition--;
        }
    }

    public boolean hasNext(){
        boolean sub = subIterator!=null&&subIterator.hasNext();
        return sub||!sub&& hasExtraNext();
    }

    public int[][] generateNext(){
        if(subIterator==null||!subIterator.hasNext()){
            subIterator = generateExtraNext().iterator();
        }else iteratorPosition++;
        Integer nextNumber = subIterator.next();
        int to = cash.getLast()+1+step;
        int nextSize;
        if(to<=totalSize)nextSize = to;
        else nextSize = totalSize;
        int[][] out = new int[nextSize-(cash.getLast()+1)][2];
        for(int x=cash.getLast()+1, y=0; x<nextSize; x++, y++){
            out[y][0] = nextNumber.intValue();
            out[y][1] = x;
        }
        combintaionsElapsed+=out.length;
        return out;
    }

    public long getCombintaionsElapsed(){
        return combintaionsElapsed;
    }

    public long getCombinationsSize(){
        System.out.println("Getting combinations size");
        Mathematics mathematics = new Mathematics();
        long out = mathematics.getCombination(totalSize);
        System.out.println("Total combinations " + out);
        return out;
    }

    @Override
    public Iterator<int[][]> iterator() {
        Iterator<int[][]> out = new Iterator<int[][]>() {

            @Override
            public boolean hasNext() {
                return Generator.this.hasNext();
            }

            @Override
            public int[][] next() {
                return Generator.this.generateNext();
            }
        };
        return out;
    }
    private static class Mathematics implements Serializable {

        public long getCombination(long sourceSize){
            long out;
            BigInteger factorial1 = factorial(sourceSize);
            BigInteger factorial2 = factorial(sourceSize-2).multiply(BigInteger.valueOf(2));
            out = factorial1.divide(factorial2).longValue();
            return out;
        }

        public BigInteger factorial(long n){
            BigInteger fact = BigInteger.valueOf(1);
            for (long i = 1; i <= n; i++)
                fact = fact.multiply(BigInteger.valueOf(i));
            return fact;
        }


    }

}
