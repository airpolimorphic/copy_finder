package model.unit;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

//todo refactor it - too many copies of code
public class PlayTime {
    //imported from EvilPlayer
    private long time;
    private final long SECOND = 1000;
    private final long MINUTE = SECOND*60;
    private final long HOUR = MINUTE*60;
    private final long DAY = HOUR*24;

    public PlayTime(int time){
        this.time = (long)time;
    }
    public PlayTime(long time){
        this.time = time;
    }
    public String getDisplayedTime(){
        StringBuilder outputLine = new StringBuilder();
        if(hasDay()){
            outputLine
                     .append(getDay())
                     .append(" d\r\n")
                    .append(getHour())
                    .append(":")
                    .append(getMinute())
                    .append(":")
                    .append(getSecond())
            ;
        }
        else if(hasHour()){
            outputLine
                    .append(getHour())
                    .append(":")
                    .append(getMinute())
                    .append(":")
                    .append(getSecond())
            ;
        }
        else if(hasMinute()){
            outputLine
                    .append(getMinute())
                    .append(":")
                    .append(getSecond())
            ;
        }
        else if(hasSecond()) {
            outputLine
                    .append(getMinute())
                    .append(":")
                    .append(getSecond())
            ;
        }else{
            outputLine
                    .append("00")
            ;
        }
        return outputLine.toString();
    }
    public String getDisplayedTimeAtLine(){
        StringBuilder outputLine = new StringBuilder();
        if(hasDay()){
            outputLine
                    .append(getDay())
                    .append(" d ")
                    .append(getHour())
                    .append(":")
                    .append(getMinute())
                    .append(":")
                    .append(getSecond())
            ;
        }
        else if(hasHour()){
            outputLine
                    .append(getHour())
                    .append(":")
                    .append(getMinute())
                    .append(":")
                    .append(getSecond())
            ;
        }
        else if(hasMinute()){
            outputLine
                    .append(getMinute())
                    .append(":")
                    .append(getSecond())
            ;
        }
        else if(hasSecond()) {
            outputLine
                    .append(getMinute())
                    .append(":")
                    .append(getSecond())
            ;
        }else{
            outputLine
                    .append("00")
            ;
        }
        return outputLine.toString();
    }

    public String getDisplayedDate(){
        Date date = new Date(time);
        DateFormat format2 = new SimpleDateFormat("yyyy.MM.dd\r\nhh:mm a");
        return format2.format(date);
    }

    private boolean hasSecond(){
        if(time>=SECOND) return true;
        return false;
    }
    private boolean hasMinute(){
        if(time>=MINUTE) return true;
        return false;
    }
    private boolean hasHour(){
        if(time>=HOUR) return true;
        return false;
    }
    private boolean hasDay(){
        if(time>=DAY) return true;
        return false;
    }
    private String getDay(){
        String out;

        try {
            long result = time / DAY;
            time = time - result * DAY;
            if (result < 10) out = "0" + result;
            else out = result + "";
        }catch(Exception e){
            out = "err division by zero";
        }

        return out;
    }
    private String getHour(){
        String out;
        try{
            long result = time/HOUR;
            time = time - result*HOUR;
            if(result<10)out = "0"+result;
            else out = result+"";
        }catch(Exception e){
            out = "err division by zero";
        }
        return out;
    }
    private String getMinute(){
        String out;
        try{
            long result = time/MINUTE;
            time = time - result*MINUTE;
            if(result<10)out = "0"+result;
            else out = result+"";
        }catch(Exception e){
            out = "err division by zero";
        }
        return out;
    }
    private String getSecond(){
        String out;
        try{
            long result = time / SECOND;
            time = time - result * SECOND;
            if (result < 10) out = "0" + result;
            else out = result + "";
        }catch(Exception e){
            out = "err division by zero";
        }
        return out;
    }
}
