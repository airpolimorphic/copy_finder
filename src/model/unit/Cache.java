package model.unit;

import model.utils.ComparableImage;
import model.utils.FileUtil;
import model.utils.ImageUtil;
import view.Progress;

import java.io.*;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Cache {
    private static final String PATH = "./cache";
    private static final String SYS_TMP_FILE = "./cachetmp";

    private Map<String, String> realImageToCache = new ConcurrentHashMap<>();

    private FileUtil fUtil = new FileUtil();

    public Cache() {
        new File(PATH).mkdirs();
    }

    public void resizeAndSaveOnDisk(List<File> sourceImages, int cores) {
        clearThrash();
        ComparableImage tool = new ComparableImage();
        tool.setMinTrashold(6);

        Progress progress = new Progress();
        progress.setSize(sourceImages.size());
        ExecutorService es = Executors.newFixedThreadPool(cores);
        System.out.println("Loading temp");
        loadTmp();

        System.out.println("Saving caches");
        for (File file : sourceImages) {
            es.submit(()->{

                        if(!realImageToCache.containsKey(file.getAbsolutePath())) {
                            ComparableImage cache = tool.getResizedImageArray(file, new ImageUtil());
                            String path = saveImageCache(cache);
                            realImageToCache.put(file.getAbsolutePath(), path);
                            if(realImageToCache.size()%1000==0){
                                StringBuilder sb = new StringBuilder();
                                for(Map.Entry<String, String> kv:realImageToCache.entrySet()){
                                    sb.append(kv.getKey()).append("\t").append(kv.getValue()).append("\r\n");
                                }
                                fUtil.write(sb.toString(), SYS_TMP_FILE, true);
                            }

                        }
                    progress.step();
            });
        }
        es.shutdown();
        try {
            es.awaitTermination(365, TimeUnit.DAYS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        StringBuilder sb = new StringBuilder();
        for(Map.Entry<String, String> kv:realImageToCache.entrySet()){
            sb.append(kv.getKey()).append("\t").append(kv.getValue()).append("\r\n");
        }
        fUtil.write(sb.toString(), SYS_TMP_FILE, true);

    }

    public ComparableImage load(File file) {
        return load(realImageToCache.get(file.getAbsolutePath()));
    }

    private String saveImageCache(ComparableImage image) {
        String path = this.PATH + "/" + UUID.randomUUID().toString();
        fUtil.save(image.getIntImage(), path);
        return path;
    }

    private ComparableImage load(String path) {
        ComparableImage out = new ComparableImage();
        out.setIntImage(fUtil.load(path));
        return out;
    }

    private void loadTmp() {
        try (Scanner in = new Scanner(new File(SYS_TMP_FILE))) {
            while (in.hasNextLine()) {
                String stroke = in.nextLine();
                if (stroke.isEmpty()) continue;
                String[] data = stroke.split("\t");
                realImageToCache.put(data[0], data[1]);
            }
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        }

    }

    private void clearThrash() {
        StringBuilder sb = new StringBuilder();
        try (Scanner in = new Scanner(new File(SYS_TMP_FILE))) {
            while (in.hasNextLine()) {
                String stroke = in.nextLine();
                if (stroke.isEmpty()) continue;
                String[] data = stroke.split("\t");
                if (new File(data[0]).exists()) {
                    sb.append(stroke).append("\r\n");
                }else{
                    new File(data[1]).delete();
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        }
        fUtil.write(sb.toString(), SYS_TMP_FILE, true);
    }

    public void finalizeStorage(){
        Progress progress = new Progress();
        System.out.println("Deleting tmp");
        progress.setSize(realImageToCache.size());
        for(Map.Entry<String, String> kv : realImageToCache.entrySet()){
            new File(kv.getValue()).delete();
            progress.step();
        }
        new File(SYS_TMP_FILE).delete();
        new File(PATH).delete();

    }
}
