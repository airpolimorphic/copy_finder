package model.unit;

public class AudioCore {
    private static AudioCore core = new AudioCore();

    static Audio audio;
    static Audio audio2;

    private AudioCore(){
        try {
            audio = new Audio("./sound.wav");
            audio2 = new Audio("./sound0.wav");
        }catch (Exception anyException){
            //ignore;
        }
    }

    public  void play(){
        try {
            audio.play();
        }catch (Exception anyException){
            //ignore;
        }
    }

    public void play2(){
        try{
        audio2.play();
        }catch (Exception anyException){
            //ignore, not important;
        }
    }

    public void close(){
        try{
        audio.close();
        audio2.close();
        }catch (Exception anyException){
            //ignore, not important;
        }
    }


    public static AudioCore instance(){
            return core;
    }


}
