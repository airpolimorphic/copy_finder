package model.unit;

import javax.sound.sampled.*;
import java.io.File;
import java.io.IOException;

public class Audio {


    private Clip clip;

    public Audio(String source){
        clip = load(source);
    }

    public void play(){
        playSound(clip);
    }

    public void close(){
        clip.stop();
        clip.close();
    }


    private Clip load(String source){
        try {
            File soundFile = new File(source);
            AudioInputStream ais = AudioSystem.getAudioInputStream(soundFile);
            Clip clip = AudioSystem.getClip();
            clip.open(ais);
            return clip;
        } catch (LineUnavailableException e) {
            //ignoring not important
//            e.printStackTrace();
        } catch (IOException e) {
            //ignoring not important
//            e.printStackTrace();
        } catch (UnsupportedAudioFileException e) {
            //ignoring not important
//            e.printStackTrace();
        }
        return null;
    }

    private  void playSound(Clip clip) {
        try {
            clip.setFramePosition(0);
            clip.start();
            Thread.sleep(clip.getMicrosecondLength()/1100);
            clip.stop();
        } catch (InterruptedException e) {
            //ignoring not important
//            e.printStackTrace();
        }
    }




    private static void playSound2() {
        try {
            AudioInputStream audioIn = AudioSystem.getAudioInputStream(Audio.class.getResource("./sound.wav"));
            Clip clip = AudioSystem.getClip();
            clip.open(audioIn);
            clip.start();
            Thread.sleep(clip.getMicrosecondLength() / 1000);
            clip.stop();
            clip.close();
        } catch (InterruptedException e) {
//            e.printStackTrace();
        } catch (LineUnavailableException e) {
//            e.printStackTrace();
        } catch (IOException e) {
//            e.printStackTrace();
        } catch (UnsupportedAudioFileException e) {
//            e.printStackTrace();
        }
    }
}
