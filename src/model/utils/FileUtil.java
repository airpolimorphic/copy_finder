package model.utils;

import model.exceptions.UnexpectedException;

import java.io.*;
import java.util.*;

public class FileUtil implements Serializable {
    private String settingsPath = "./settings";
    private String deleteListPath = "./delete";
    private final String WRITE_MODE = "rw";
    private final Set<String> EXCLUSION_FOLDERS = new HashSet<>(
            Arrays.asList("bin", "src")
    );

    private Deque<String> SUPPORTED_EXTENSIONS = new ArrayDeque<>(
            Arrays.asList(".jpg", ".jpeg")
    );

    public <T> void save(T data, String path) {
        try (ObjectOutputStream oos = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(path)))) {
            oos.writeObject(data);
            oos.flush();
        } catch (Exception ex) {
            throw new UnexpectedException(ex.getMessage());
        }
    }

    public <T> T load(String path) {
        T out;
        try (ObjectInputStream ois = new ObjectInputStream(new BufferedInputStream(new FileInputStream(path)))) {
            out = (T) ois.readObject();
        } catch (Exception ex) {
            new File(path).delete();
            throw new UnexpectedException(ex.getMessage());
        }
        return out;
    }

    public <T> T loadSetting(String path) {
        T out;
        try (ObjectInputStream ois = new ObjectInputStream(new BufferedInputStream(new FileInputStream(path)))) {
            out = (T) ois.readObject();
        } catch (Exception ex) {
            throw new UnexpectedException(ex.getMessage());
        }
        return out;
    }

    public ArrayList<File> getImages(String path) {
        ArrayList<File> out = new ArrayList<>();
        File[] fs = scanDir(path);
        for (File current : fs) {
            if(EXCLUSION_FOLDERS.contains(current.getName())&&current.isDirectory()) continue;
            if (current.isFile()) {
                if(isValidByExtension(current.getName())) out.add(current);
            } else out.addAll(getImages(current.getAbsolutePath()));
        }
        return out;
    }

    public File[] scanDir(String path) {
        return new File(path).listFiles();
    }

    public void addFileToDelete(File origin) {
        write("\r\n" + origin.getAbsolutePath(), deleteListPath, false);
    }

    public List<String> getDeleteList() {
        return read(deleteListPath);
    }

    public void clear() {
        new File(settingsPath).deleteOnExit();
        new File(deleteListPath).deleteOnExit();
    }

    public List<String> read(String path) {
        List<String> out = new ArrayList<>();
        if (!new File(path).exists()) return out;
        try (Scanner in = new Scanner(new File(path))) {
            while (in.hasNextLine()) {
                try {
                    String stroke = in.nextLine();
                    if (stroke.isEmpty()) continue;
                    out.add(stroke);
                } catch (NumberFormatException e) {
                    System.out.println(e.getMessage());
                    continue;
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
            return out;
        }
        return out;
    }

    public void write(String data, String destination, boolean rewrite) {
        String root = destination.substring(0, destination.lastIndexOf(File.pathSeparator) + 1);
        (new File(root)).mkdirs();
        try(RandomAccessFile file = new RandomAccessFile(destination, WRITE_MODE)){
            if(!rewrite) file.seek(file.length());
            file.write(data.getBytes());
            file.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private boolean isValidByExtension(String name){
        for(String extension:SUPPORTED_EXTENSIONS){
            extension = extension.toLowerCase();
            if (name.endsWith(extension)) return true;
        }
        return false;
    }
}
