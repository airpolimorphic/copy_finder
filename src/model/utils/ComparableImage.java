package model.utils;

import model.exceptions.UnexpectedException;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;


//todo make normal code, because here is chaos
public class ComparableImage {
    private static final int RANGE_MIN = 75;
    private static final int RANGE_MAX = 100;
    private static final int PIXEL_DIFF = 25;

    private File path;
    private BufferedImage bufferedImage;
    private ImageUtil util;
    private int[][] intImage;

    public static final int MIN_TRASHHOLD = 240;

    private int minTrashold = 240;
    public ComparableImage(){

    }

    public ComparableImage getResizedImageArray(File path, ImageUtil util){
        ComparableImage image = new ComparableImage(path, util, true, minTrashold);
        return image;
    }




    public ComparableImage(File path, ImageUtil util, boolean calculateArray, int minTrashold){
        this.path = path;
        this.util = util;
        try {
            bufferedImage = util.getImage(path);



            if(calculateArray) {
                boolean resize = false;
                int width = bufferedImage.getWidth();
                int height = bufferedImage.getHeight();
//                System.out.println("was "+width+"x"+height);
                if(width!=minTrashold){
                    width = minTrashold;
                    resize = true;
                }
                if(height!=minTrashold){
                    height = minTrashold;
                    resize = true;
                }
                if(resize){
//                    System.out.println("resizing "+width+"x"+height);
                    ComparableImage image = resize(width, height);
                    bufferedImage = image.bufferedImage;
                    intImage = image.getIntImage();
                }else intImage = util.getGrayIntArr(bufferedImage);
            }
        }catch (IOException e){
            throw new UnexpectedException(e.getMessage());
        }
    }

    public void setMinTrashold(int minTrashold){
        this.minTrashold = minTrashold;
    }

    private ComparableImage(ComparableImage image, BufferedImage resizedImage){
        bufferedImage = resizedImage;
        path = image.path;
        util = image.util;
        intImage = util.getGrayIntArr(bufferedImage);
    }

    public int[][] getIntImage(){
        return intImage;
    }

    public void setIntImage(int[][] image){
        intImage = image;
    }

    private ComparableImage resize(int width, int height){
        BufferedImage image = util.aggressiveResizeImage(bufferedImage, width, height);
        ComparableImage comparableImage = new ComparableImage(this, image);
        return comparableImage;
    }

    public boolean equals(ComparableImage image){
        int[][] imageA = intImage;
        int percent = getEqualsPercentage(imageA, image.getIntImage());
        boolean out = percent >= RANGE_MIN && percent <= RANGE_MAX;
        return out;
    }

    private int getEqualsPercentage(int[][] img1, int[][] img2){
        int out = 0;
        if(img1.length!=img2.length) throw new RuntimeException("Different size! Wid1: "+img1.length+", Wid2: "+img2.length);
        for(int i=0; i<img1.length; i++){
            if(img1[i].length!=img2[i].length)throw new RuntimeException("Images sizes is different");
            for(int y=0; y<img2[i].length; y++){
                if(diff(img1[i][y], img2[i][y])<=PIXEL_DIFF) out++;
            }
        }
        out = (out * 100) / (img1.length*img1[0].length);
        return out;
    }

    private int diff(int a, int b){
        int c = a  - b;
        if(c<0)c = c*(-1);
        return c;
    }


}
