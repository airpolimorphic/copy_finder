package model.utils;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;

public class ImageUtil {

    public Color getPixelColor(BufferedImage bi, int posX, int posY){
        return new Color( bi.getRGB(posX, posY), true);
    }

    public int[][] getGrayIntArr(BufferedImage image){
        int out[][] = new int[image.getWidth()][image.getHeight()];
        for(int x=0; x<image.getWidth(); x++){
            for(int y=0; y<image.getHeight(); y++){
                Color color = getPixelColor(image, x, y);
                int gray = (color.getRed()+color.getGreen()+color.getBlue())/3;
                out[x][y]=gray;
            }
        }
        return out;
    }

    public BufferedImage aggressiveResizeImage(BufferedImage input, int width, int height){
        ImageIcon img = new ImageIcon(input);
        Image mg = img.getImage().getScaledInstance(width, height, Image.SCALE_REPLICATE);
        return toBufferedImage(mg);
    }


    public BufferedImage getImage(File input)throws IOException{
        String extension = getFileExtension(input);
        BufferedImage image = toBufferedImage(input, extension);
        return image;
    }

    public String getFileExtension(File file) {
        String fileName = file.getName();
        if(fileName.contains(".") && fileName.lastIndexOf(".") != 0)
            return fileName.substring(fileName.lastIndexOf(".")+1);
        throw new RuntimeException("Extension not found");
    }

    private BufferedImage toBufferedImage(Image image){
        if (image instanceof BufferedImage) return (BufferedImage) image;
        image = new ImageIcon(image).getImage();
        BufferedImage bufferedImage = new BufferedImage(image.getWidth(null),image.getHeight(null),
                BufferedImage.TYPE_INT_RGB);
        Graphics g = bufferedImage.createGraphics();
        g.drawImage(image,0,0,null);
        g.dispose();
        return bufferedImage;
    }

    private BufferedImage toBufferedImage(File file, String extension)throws IOException{
       try(ImageInputStream imageIn = ImageIO.createImageInputStream(file)){
           Iterator<ImageReader> iterator = ImageIO.getImageReadersByFormatName(extension);
           ImageReader reader = iterator.next();
           reader.setInput(imageIn);
           BufferedImage image = reader.read(0);
           return image;
       }
    }

}
