package model;

import model.exceptions.UnexpectedException;
import model.unit.AudioCore;
import model.unit.Cache;
import model.unit.Generator;
import model.utils.ComparableImage;
import model.utils.FileUtil;
import model.utils.ImageUtil;
import view.Progress;
import view.View;
import view.data.provider.Result;
import view.data.provider.State;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;


//TODO make refactor for this class because here happens bullshit
public class CopyDetector {
    private AtomicInteger counterA = new AtomicInteger(0);
    private AtomicInteger counterMatches = new AtomicInteger(0);
    private AtomicLong counterMatchesSize = new AtomicLong();
    private AtomicLong deletedFilesSize = new AtomicLong();
    private AtomicInteger deletedFiles = new AtomicInteger();
    private AtomicInteger errorsCount = new AtomicInteger();


    private int numThreads = 4;
    private int numUnitsInBlock = 4000;
    private boolean deleteCopies = false;

    private FileUtil fUtil = new FileUtil();

    private final String GROUP_PATH = "./groups";
    private final String GROUP_PATH2 = "./groupsReserv";

    Map<File, ComparableImage> fastCache = new ConcurrentHashMap<>();

    private ExecutorService systemInThread = Executors.newSingleThreadExecutor();
    private boolean isExit = false;

    public CopyDetector setDeleteCopies(boolean deleteCopies) {
        this.deleteCopies = deleteCopies;
        return this;
    }

    public CopyDetector setNumThreads(int numThreads) {
        if (numThreads > 0) {
            this.numThreads = numThreads;
        }
        return this;
    }

    public CopyDetector setNumUnitsInBlock(int numUnitsInBlock) {
        if (numUnitsInBlock > 0) {
            this.numUnitsInBlock = numUnitsInBlock;
        }
        return this;
    }

    public void start(final List<File> sourceF, String destinationFolder, FileUtil generalSettings) {
        State state = new State();
        Result result  = new Result();
        Cache cache = new Cache();

        cache.resizeAndSaveOnDisk(sourceF, numThreads);
        new File(destinationFolder).mkdirs();

        //todo move to view
        System.out.println("Total Files " + sourceF.size());

        ExecutorService es = Executors.newFixedThreadPool(numThreads);
        counterA.set(0);
        Map<File, Set<File>> toDelete = new ConcurrentHashMap<>();


        String pathProgress = "./progress";
        Generator generator = new Generator();
        generator.setTotalSize(sourceF.size());
        generator.setBlockSize(numUnitsInBlock);
        state.setTotalCombinations(generator.getCombinationsSize());

        try {
            generator = fUtil.load(pathProgress);
            generator.load();
        } catch (RuntimeException e) {
            //all ok
            System.out.println(e.getMessage());
        }

        state.setElapsedCombinations(generator.getCombintaionsElapsed());

        boolean fromFastCache = numUnitsInBlock >= sourceF.size();
        Set<String> copyesBuffer = Collections.synchronizedSet(new HashSet<>());

        int counterOfIterations = 0;
        long timeStamp = System.currentTimeMillis();

        for (int[][] comparableArray : generator) {

            Set<File> changedCopies = new HashSet<>();

            int startMatches = counterMatches.get();
            state.setCurrentCombinations(comparableArray.length);

            state.setSpentTime(System.currentTimeMillis()-timeStamp);
            timeStamp = System.currentTimeMillis();

            View.instance().print(state);

            Map<File, ComparableImage> bufferClone;
            if (fromFastCache && !fastCache.isEmpty()) {
                bufferClone = fastCache;
            } else {
                Set<File> toLoad = prepare(comparableArray, sourceF);
                //todo move to view
                state.show("Loading images");
                bufferClone = load(toLoad, cache, fromFastCache);
                toLoad.clear();
                toLoad = null;
            }

            state.show("Comparing");
            if (counterOfIterations % 150 == 0) {
                System.gc();
            }
            state.show("\r\nPreparing is done");

            Progress progress = new Progress();
            progress.setSize(comparableArray.length);

            for (int[] unit : comparableArray) {

                File firstFile = sourceF.get(unit[0]);
                File secondFile = sourceF.get(unit[1]);



                es.submit(() -> {
                    try {
//                            state.show("Comparing: " + model.unit[0] + "-" + model.unit[1]);
                        progress.step();
                        ComparableImage imageA = bufferClone.get(firstFile);
                        ComparableImage imageB = bufferClone.get(secondFile);
                        if (imageA.equals(imageB)) {
                            ImageUtil util = new ImageUtil();
                            ComparableImage image1 = new ComparableImage(firstFile, util, true, ComparableImage.MIN_TRASHHOLD);
                            ComparableImage image2 = new ComparableImage(secondFile, util, true, ComparableImage.MIN_TRASHHOLD);
                            if (!image1.equals(image2)) {
                                errorsCount.incrementAndGet();
                                return;
                            }

                            deletedFiles.incrementAndGet();
                            deletedFiles.incrementAndGet();
                            deletedFilesSize.addAndGet(secondFile.length());
                            deletedFilesSize.addAndGet(firstFile.length());
                            generalSettings.addFileToDelete(firstFile);
                            generalSettings.addFileToDelete(secondFile);

                            counterMatches.incrementAndGet();
                            Set<File> copyes;
                            if (!toDelete.containsKey(firstFile)) {
                                copyes = new HashSet<>();
                            } else {
                                copyes = toDelete.get(firstFile);
                            }
                            copyes.add(secondFile);
                            toDelete.put(firstFile, copyes);
                            changedCopies.add(firstFile);
                            changedCopies.add(secondFile);

                            counterMatchesSize.addAndGet(firstFile.length());
                            counterMatchesSize.addAndGet(secondFile.length());
                        }
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                });
            }
            shutdownAndWait(es);


            es = Executors.newFixedThreadPool(numThreads);
            //region COPY
            if (startMatches != counterMatches.get()) {
                reorganize(toDelete);
                if (!deleteCopies) {
                    for (Map.Entry<File, Set<File>> kv : toDelete.entrySet()) {
                        if (!kv.getKey().exists()) continue;
                        if (kv.getValue().size() == 0) continue;
                        if(!changedCopies.contains(kv.getKey()))continue;
                        File destination = new File(destinationFolder + "/" + kv.getKey().getName());
                        try {
                            if (destination.exists()) continue;
                            Files.copy(kv.getKey().toPath(), destination.toPath());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        for (File copy : kv.getValue()) {
                            if (!copy.exists()) continue;
                            String name = copy.getName();
                            String extension = name.substring(name.lastIndexOf("."));
                            name = name.replaceFirst(extension, "");
                            String baseName = kv.getKey().getName().substring(kv.getKey().getName().lastIndexOf("."));
                            baseName = kv.getKey().getName().replaceFirst(baseName, "");
                            destination = new File(destinationFolder + "/" + baseName + "-" + name + extension);
                            if (destination.exists()) continue;
                            try {
                                Files.copy(copy.toPath(), destination.toPath());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                } else {
                    //todo make an interface CopyExecutor, code under - is one of implementation
                    Map<File, File> replacementSet = new HashMap<>();
                    for (Map.Entry<File, Set<File>> kv : toDelete.entrySet()) {
                        if (!kv.getKey().exists()) continue;
                        if (kv.getValue().size() == 0) continue;
                        boolean skip = !changedCopies.contains(kv.getKey());

                        if(skip){
                            for(File file:kv.getValue()){
                                if(changedCopies.contains(file)) {
                                    skip = false;
                                    break;
                                }
                            }
                        }

                        if(skip) continue;

                        List<File> copyesArray = new ArrayList<>();
                        File source = kv.getKey();
                        copyesArray.add(source);
                        for (File copy : kv.getValue()) {
                            if (!copy.exists()) {
                                continue;
                            }
                            copyesArray.add(copy);
                        }
                        //todo make replacement slow speed
                        File copy = selectMostCoolCopy(copyesArray);
                        if (copyesBuffer.contains(copy.getAbsolutePath())) {
                            continue;
                        } else copyesBuffer.add(copy.getAbsolutePath());
                        //clear history for fast actual search
                        replacementSet.put(kv.getKey(), copy);

                        File destination = new File(destinationFolder + "/" + copy.getName());
                        if (destination.exists()) destination.delete();

                        try {
                            Files.copy(copy.toPath(), destination.toPath());
                            deletedFiles.decrementAndGet();
                            deletedFilesSize.set(deletedFilesSize.get()-copy.length());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    //clear history for fast actual search
                    for (Map.Entry<File, File> kv : replacementSet.entrySet()) {
                        toDelete.remove(kv.getKey());
                        toDelete.put(kv.getValue(), new HashSet<>());
                    }

                }
            }
            //endregion

            result.setTotalMaches(counterMatches.get());
            result.setTotalMachesSize(counterMatchesSize.get());
            result.setFilesDeleted(deletedFiles.get());
            result.setDeletedFileSize(deletedFilesSize.get());
            result.setErrorsCount(errorsCount.get());
            result.print();

            AudioCore.instance().play2();
            fUtil.save(generator, pathProgress);
            counterOfIterations++;
        }
        shutdownAndWait(es);
        new File(GROUP_PATH).delete();
        new File(GROUP_PATH2).delete();
        new File(pathProgress).delete();
        toDelete.clear();
        cache.finalizeStorage();
    }


    private int[] getDimensions(File path) {
        ImageUtil util = new ImageUtil();
        int[] out = new int[2];
        out[0] = 0;
        out[1] = 1;
        try {
            BufferedImage image = util.getImage(path);
            out[0] = image.getWidth();
            out[1] = image.getHeight();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return out;

    }

    private File selectMostCoolCopy(List<File> data) {
        File out = null;
        int lastDimension = 0;
        long size = Long.MAX_VALUE;
        for (File file : data) {
            if (!file.exists()) continue;
            int[] currentDimension = getDimensions(file);
            int summ = currentDimension[0] + currentDimension[1];
            long fileSize = file.length();
            if (summ > lastDimension) {
                lastDimension = summ;
                size = fileSize;
                out = file;
            } else if (summ == lastDimension) {
                if (fileSize > size) {
                    out = file;
                    size = fileSize;
                }
            }
        }
        return out;
    }


    private Map<File, Set<File>> reorganize(Map<File, Set<File>> data) {
        List<File> remove = new ArrayList<>();
        for (Map.Entry<File, Set<File>> keyValue : data.entrySet()) {
            File key = keyValue.getKey();
            List<File> currentList = new ArrayList<>();
            for (File copy : keyValue.getValue()) {
                if (data.containsKey(copy)) currentList.add(copy);
            }
            for (File key2 : currentList) {
                Set<File> block2 = data.get(key2);
                block2.remove(key);
                keyValue.getValue().addAll(block2);
            }
            remove.addAll(currentList);
        }
        for (File toRemove : remove) data.remove(toRemove);
        return data;
    }

    private Set<File> prepare(int[][] data, List<File> source) {
        Set<File> out = Collections.synchronizedSet(new HashSet<File>());
        for (int[] indexes : data) {
            out.add(source.get(indexes[0]));
            out.add(source.get(indexes[1]));
        }
        return out;
    }

    private Map<File, ComparableImage> load(final Set<File> data, Cache cache, boolean fromFastCache) {

        if (fromFastCache && !fastCache.isEmpty()) {
            return fastCache;
        }

        Map<File, ComparableImage> out = new ConcurrentHashMap<>();
        ExecutorService es = Executors.newFixedThreadPool(numThreads);
        Progress progress = new Progress();
        progress.setSize(data.size());

        for (final File file : data) {
            es.submit(() -> {
                progress.step();
                if (!fromFastCache || fastCache.isEmpty()) {
                    try {
                        ComparableImage image = null;
                        image = cache.load(file);
                        out.put(file, image);
                    } catch (UnexpectedException e) {
                        new File("./virus").mkdirs();
                        file.renameTo(new File("./virus/" + file.getName()));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });
        }
        shutdownAndWait(es);
        if (fastCache.isEmpty()) fastCache = out;
        return out;
    }

    private void shutdownAndWait(ExecutorService es) {
        es.shutdown();
        try {
            es.awaitTermination(365, TimeUnit.DAYS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
